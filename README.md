# daverona/docker-compose/swaggered

## Prerequisites

* `swaggered.example` mapped to `127.0.0.1` &mdash; check [this](https://gitlab.com/daverona/docker-compose/dnsmasq) out
* [Traefik](https://docs.traefik.io/) &mdash; check [this](http://gitlab.com/daverona/docker-compose/traefik) out

## Quick Start

```bash
cp .env.example .env  
# edit .env
docker-compose up --detach
```

Visit [http://swaggered.example](http://swaggered.example).

## References

* OpenAPI Documentation: [https://swagger.io/docs/specification/](https://swagger.io/docs/specification/)
* OpenAPI Specification: [https://swagger.io/specification/](https://swagger.io/specification/)
* Swagger Editor repository: [https://github.com/swagger-api/swagger-editor](https://github.com/swagger-api/swagger-editor)
* Swagger Editor registry: [https://hub.docker.com/r/swaggerapi/swagger-editor](https://hub.docker.com/r/swaggerapi/swagger-editor)
